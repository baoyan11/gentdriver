<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>GentDriver Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jqpagination.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/back.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
	<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/javascript/jquery.jqpagination.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
	
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<aside id="sidebar" class="column">
	  	<jsp:include page="navbar.jsp"></jsp:include>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<table id="regulationTable" class="table table-striped" cellspacing="0" width="100%">
			<thead>
	            <tr>
	                <th style="width:7%;">车牌号</th>
	                <th style="width:7%;">车辆类型</th>
	                <th style="width:10%;">凭证编号</th>
	                <th style="width:10%;">违法时间</th>
	                <th style="width:15%;">违法地点</th>
	                <th style="width:15%;">采集机关</th>
	                <th style="width:15%;">违法内容</th>
	                <th style="width:15%;">违反条款</th>
	                <th style="width:6%;">状态</th>
	            </tr>
	        </thead>
	        <tbody id="regulationTbody">
			</tbody>
		</table>
		<div class="pagination" style="float:right;">
		    <a href="#" class="first" data-action="first">&laquo;</a>
		    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
		    <input type="text" readonly="readonly" data-max-page="40" />
		    <a href="#" class="next" data-action="next">&rsaquo;</a>
		    <a href="#" class="last" data-action="last">&raquo;</a>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function() {
		    var regulationList = ${regulations}.results;
			var regulationbody = '';
			for(var i = 0; i < regulationList.length; i ++) {
				var regulation = regulationList[i];
				regulationbody += '<tr>' +
	                    			'<td>' + regulation.cardArea + regulation.cardNumber + '</td>' +
	                    			'<td>' + regulation.cardType + '</td>' + 
	                    			'<td>' + regulation.seq + '</td>' +
	                    			'<td>' + regulation.time + '</td>' +
	                    			'<td>' + regulation.address + '</td>' +
	                    			'<td>' + regulation.dept + '</td>' +
	                    			'<td>' + regulation.content + '</td>' +
	                    			'<td>' + regulation.rule + '</td>' +
	                    			'<td>' + regulation.status + '</td>' +
		                  		'</tr>';
			}
			$('#regulationTbody').append(regulationbody);
		
		    $('.pagination').jqPagination({
			    paged: function(page) {
			        $.ajax( {
				      url: 'regulation',
				      type: 'POST',
				      data: {pages:page,pageSize:10},
				      success:function(msg){
				      	if(msg.results != null){
							$('#regulationTbody').html('');
							var regulationbody = '';
							var regulationList = msg.results;
							for(var i = 0; i < regulationList.length; i ++) {
								var regulation = regulationList[i];
								regulationbody += '<tr>' +
	                    			'<td>' + regulation.cardArea + regulation.cardNumber + '</td>' +
	                    			'<td>' + regulation.cardType + '</td>' + 
	                    			'<td>' + regulation.seq + '</td>' +
	                    			'<td>' + regulation.time + '</td>' +
	                    			'<td>' + regulation.address + '</td>' +
	                    			'<td>' + regulation.dept + '</td>' +
	                    			'<td>' + regulation.content + '</td>' +
	                    			'<td>' + regulation.rule + '</td>' +
	                    			'<td>' + regulation.status + '</td>' +
		                  		'</tr>';
							}
							$('#regulationTbody').append(regulationbody);
				      	}
				      }
				    });
				},
				max_page: ${regulationPages}
			});
		});
		
	</script>
</body>

</html>