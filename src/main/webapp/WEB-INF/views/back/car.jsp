<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>GentDriver Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jqpagination.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/back.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
	<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/javascript/jquery.jqpagination.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
	
</head>

<body>
	oat<jsp:include page="header.jsp"></jsp:include>
	
	<aside id="sidebar" class="column">
	  	<jsp:include page="navbar.jsp"></jsp:include>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<table id="carTable" class="table table-striped" cellspacing="0" width="100%">
			<thead>
	            <tr>
	                <th>车牌号</th>
	                <th>车辆类型</th>
	                <th>发动机号</th>
	                <th>真实性</th>
	            </tr>
	        </thead>
	        <tbody id="carTbody">
			</tbody>
		</table>
		<div class="pagination" style="float:right;">
		    <a href="#" class="first" data-action="first">&laquo;</a>
		    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
		    <input type="text" readonly="readonly" data-max-page="40" />
		    <a href="#" class="next" data-action="next">&rsaquo;</a>
		    <a href="#" class="last" data-action="last">&raquo;</a>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function() {
			var carList = ${cars}.results;
			var carbody = '';
			for(var i = 0; i < carList.length; i ++) {
				var car = carList[i];
				carbody += '<tr>' +
		                    '<td>' + car.cardArea + car.cardNo + '</td>' +
		                    '<td>' + car.cardType + '</td>' +
		                    '<td>' + car.machineNo + '</td>' +
		                    '<td>' + car.isReal + '</td>' +
		                  '</tr>';
			}
			$('#carTbody').append(carbody);
		
		    $('.pagination').jqPagination({
			    paged: function(page) {
			        $.ajax( {
				      url: 'car',
				      type: 'POST',
				      data: {pages:page,pageSize:10},
				      success:function(msg){
				      	if(msg.results != null){
							$('#carTbody').html('');
							var carbody = '';
							var carList = msg.results;
							for(var i = 0; i < carList.length; i ++) {
								var car = carList[i];
								carbody += '<tr>' +
						                    '<td>' + car.cardArea + car.cardNo + '</td>' +
						                    '<td>' + car.cardType + '</td>' +
						                    '<td>' + car.machineNo + '</td>' +
						                    '<td>' + car.isReal + '</td>' +
						                  '</tr>';
							}
							$('#carTbody').append(carbody);
				      	}
				      }
				     });
			    },
			    max_page: ${carPages}
			});
		});
	</script>
</body>

</html>