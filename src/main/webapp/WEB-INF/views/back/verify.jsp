<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>GentDriver Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/back.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/gallery.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/load-more.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
	<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/javascript/gallery-scroll.js"></script>
	<script src="<%=request.getContextPath()%>/javascript/unslider.js"></script>
	<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
	
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<aside id="sidebar" class="column">
	  	<jsp:include page="navbar.jsp"></jsp:include>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<div class="gallery-thumbnail" style="width:85%;">
			<div id="gallery-result"></div>
			<div class="row">
				<span style="display:block; text-align:center;margin-bottom: 15px;">
					<button style="width: 108px;" type="button" id="load_more" class="getmore btn btn-default btn-outline btn-back-white smooth color" data-page="1" data-text="更 多" data-loading-text="加载中…" data-complete-text="没有更多了">更 多</button>
				</span>
			</div>
		</div>
	</section>
	
	<section class="content2">
		<p>Copyright © 2014 gentdriver.com 沪ICP备14037150号</p>  
	</section>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content" id="gallery-detail">
	      
	    </div>
	  </div>
	</div>
	
	<div id="alertModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h4>请确认是否删除？</h4>
			    </div>
			    <div class="modal-body">
			        <div style="text-align:center;">
			        	<input type="hidden" id="delUploadInput" />
			        	<button class="btn btn-primary" style="margin-right:10%;" onclick="delConfirm();">确认</button>
			        	<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">取消</button>
			        </div>
			    </div>
		    </div>
	    </div>
	</div>
	
</body>
<script type="text/javascript">
	$.fn.transGallery=function(gallery){
		if(gallery == undefined || gallery == null){
			return "";
		}
		var table = '';
		for(var i = 0; i < gallery.length; i ++) {
			var result = gallery[i];
			
			var detail = '';
			detail += result.cardArea + result.cardNo + '/' + result.pictures.length + '/' + result.createDate + '/' + result.description + "/";
			for(j = 0; j < result.pictures.length; j++){
				detail += result.pictures[j].thumbPathLarge;
				if(j < result.pictures.length - 1)
					detail += "-";
			}
			
			if(i%4 == 0){
				table += '<div id="gallery-row" class="row">';
			}
			
			var statusStr = "";
			if(result.status == 0){
				statusStr = "未审核";
			}else if(result.status == 1){
				statusStr = "审核通过";
			}else if(result.status == 2){
				statusStr = "审核未通过";
			}
			
			table += 
				'<div class="col-xs-6 col-md-3" id="' + result.id + '-div">' + 
					'<div class="thumbnail">' + 
						//'<img data-src="holder.js/100%180" src="uploaded/' + result.pictures[0].thumbPath+ '" >' +
	                    '<img class="modalTag" index="' + i +'" data-src="holder.js/100%180" style="cursor:pointer;background-image:url(<%=request.getContextPath()%>/uploaded/'
	                          + result.pictures[0].thumbPath + ');background-repeat:no-repeat;background-position:center;height:200px;width:100%;" onclick=detail("' + detail + '") />' +
						'<p class="text1">' + result.cardArea + result.cardNo + '<span class="text4">(' + result.pictures.length + '张图)</span><span class="text3">' + result.createDate + '</span></p>' + 
						'<p class="text2" style="word-wrap:break-word; word-break:normal;overflow:auto;">' + result.description + '</p>' +
						'<fieldset style="padding-bottom:inherit;">' +
							'<p class="text1" style="text-align:center;">' + statusStr + '</p>';
			
			if(result.status == 0){
				table += '<div style="text-align:center;"><button class="btn btn-primary" style="margin-right:10%;" onclick="verify(' + result.id + ', 1);">通  过</button><button class="btn btn-danger" onclick="verify(' + result.id + ', 2);">不通过</button></div>';
			}else if(result.status == 1){
				table += '<div style="text-align:center;"><button class="btn btn-danger" onclick="verify(' + result.id + ', 2);">不通过</button></div>';
			}else if(result.status == 2){
				table += '<div style="text-align:center;"><button class="btn btn-primary" style="margin-right:10%;" onclick="verify(' + result.id + ', 1);">通  过</button><button class="btn btn-danger" onclick="del(' + result.id + ');">删  除</button></div>';
			}
			
			table +=	'</fieldset>' +
					'</div>' +
				'</div>';
			if(i%4 == 3){
				table += '</div>';
			}
		}
		return table;
	}
	$(document).ready(function(){
		var status = '${status}';
		$.ajax({
	      url: 'galleryVerify',
	      type: 'POST',
	      data: {status:status, pages:0, pageSize:8},
	      success:function(data){
	      	if(data.error != null) {
				$('#load_more').button('complete');
			} else {
				var gallery = data.results;
				var table = $(this).transGallery(gallery);
				$("#gallery-result").append(table);
				if(gallery.length < 8){
					$('#load_more').button('complete');
				}else{
					$('#load_more').attr('data-page', 1);
					$('#load_more').button('reset');
				}
			}
	      },
	      error:function(msg){
				$('#load_more').button('complete');
	      }
		});  // 奇葩展示 
		
		$('#load_more').click(function () {
			if($('#load_more').text() == $('#load_more').attr('data-complete-text')){
				return false;
			}
			$('#load_more').button('loading');
			var pageIndex = $('#load_more').attr('data-page');
		
		    $.ajax( {
		      url: 'galleryVerify',
		      type: 'POST',
		      data: {status:status, pages:pageIndex, pageSize:8},
		      success:function(data){
		      	if(data.error != null) {
					$('#load_more').button('complete');
				} else {
					var gallery = data.results;
					var table = $(this).transGallery(gallery);
					$("#gallery-result").append(table);
					$('#load_more').attr('data-page', pageIndex*1+1);
					if(gallery.length < 8){
						$('#load_more').button('complete');
					}else{
						$('#load_more').button('reset');
					}
				}
		      },
		      error:function(msg){
				$('#load_more').button('complete');
		      }
			}); // 奇葩展示
		}); 
	});
</script>
<script type="text/javascript"> 
	function detail(result){
	    $("#gallery-detail").html('');
	    
	    var results = result.split("/");
	    var pics = results[4].split("-");
	    
		var detail = 	'<div class="modal-header">' + 
	        			'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + 
	        			'<h4 class="modal-title" id="myModalLabel">' + results[0] + ' (' + results[1] + '张图)<span class="text5">' + results[2] + '</span></h4>' +
	      			'</div>' +
	      			'<div class="modal-body">' +
						'<div class="banner has-dots" style="overflow: hidden; width: 570px;">' +
                			'<ul style="position: relative; left: -100%; height: 400px; padding-left:0px;">';
        for(var i = 0; i < pics.length; i++){
			detail += '<li style="height:400px;background-image:url(<%=request.getContextPath()%>/uploaded/' + pics[i] + ');background-repeat:no-repeat;background-position:center;background-size:contain;"></li>'
        }
        detail +=			'</ul>' +
						'</div>' +
	      			'</div>' +
	      			'<div class="modal-footer">' +
	      				'<p style="text-align:left;overflow:auto;">' + results[3] + '</p>' + 
	     			'</div>';
	    $("#gallery-detail").append(detail);
	    $("#myModal").modal();
	     
		$('.banner').unslider({
			speed: 500,               //  The speed to animate each slide (in milliseconds)
			delay: 3000,              //  The delay between slide animations (in milliseconds)
			complete: function() {},  //  A function that gets called after every slide animation
			keys: true,               //  Enable keyboard (left, right) arrow shortcuts
			dots: true,               //  Display dot navigation
			fluid: false              //  Support responsive design. May break non-responsive designs
		});
	} 
	function verify(id, status){
		$.ajax( {
		      url: '<%=request.getContextPath()%>/back/verify',
		      type: 'POST',
		      data: {uploadID:id, uploadStatus:status},
		      success:function(data){
		      	if(data.error != null) {
				} else {
					$('#' + id + '-div').fadeOut('slow');
				}
		      },
		      error:function(msg){
				$('#load_more').button('complete');
		      }
			}); // 奇葩状态修改
		
	} 
	function del(id){
		$('#delUploadInput').val(id);
		$('#alertModal').modal();
	}
	function delConfirm(){
		$.ajax( {
		      url: '<%=request.getContextPath()%>/back/delUpload',
		      type: 'POST',
		      data: {id:$('#delUploadInput').val()},
		      success:function(data){
		      	if(data.error != null) {
				} else if(data.result == "OK"){
					$('#alertModal').modal('hide');
					$('#' + $('#delUploadInput').val() + '-div').fadeOut('slow');
				}
		      },
		      error:function(msg){
				$('#load_more').button('complete');
		      }
			}); // 奇葩删除
		
	} 
    </script> 

</html>