<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<header id="header">
<hgroup>
<h1 class="site_title">
	<a href="../index">Go to Website</a>
</h1>
<h2 class="section_title">
	GentDriver Management
</h2>
<div class="btn_view_site">
	<label id="logout">
		Logout
	</label>
</div>
</hgroup>
</header>

<section id="secondary_bar">
<div class="user">
	<p>
		${sessionScope.Manager.username}
		<label id="change" style="cursor: pointer;">
			修改密码
		</label>
	</p>
</div>
</section>

<div class="modal fade" id="changeModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<form id="changeForm" class="form-horizontal">
				<div class="form-group">
					<label for="oldPass" class="col-sm-2 control-label">
						原密码
					</label>
					<div class="col-sm-9">
						<input id="oldPass" name="oldPass" type="password" placeholder="请输入原密码"
							class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="newPass" class="col-sm-2 control-label">
						新密码
					</label>
					<div class="col-sm-9">
						<input id="newPass" name="newPass" type="password" placeholder="请输入新密码"
							class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label for="confirmPass" class="col-sm-2 control-label">
						再次确认
					</label>
					<div class="col-sm-9">
						<input id="confirmPass" name="confirmPass" type="password" placeholder="请再次输入新密码"
							class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-9">
						<label id="change-btn" class="btn btn-primary"
							data-loading-text="提交中...">
							提交
						</label>
						<button class="btn btn-default" type="reset">
							重置
						</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"> 
	$(document).ready(function(){
		var messenger = Messenger({extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',theme:'ice'}).post().hide();
	    $('#change').click(function () {
	    	$('#changeModal').modal(); 
	    	// 上传奇葩
				$('#change-btn').click(function () {
					
					var btn = $(this);
				    btn.button('loading');
				    
				    if($('#oldPass').val() == '' || $('#newPass').val() == '' || $('#confirmPass').val() == ''){
				   		messenger.update({
							message: '请填写完整',
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
				   		btn.button('reset');
						return false;
				    }else if($('#newPass').val() != $('#confirmPass').val()){
						messenger.update({
							message: '两次密码不一致，请确认',
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
				   		btn.button('reset');
						return false;
					}else if($('#oldPass').val() == $('#newPass').val()){
						messenger.update({
							message: '修改密码不得与原密码相同，请确认',
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
				   		btn.button('reset');
						return false;
					}
					
				    $.ajax( {
				      url: 'changePass',
				      type: 'POST',
				      data: {oldPass:$('#oldPass').val(), newPass:$('#newPass').val()},
				      success:function(msg){
				      if(msg.result != null){
				      		messenger.update({
								message: msg.result,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
							if(msg.code == 1){
								$('#changeModal').modal('hide');
								$('#changeForm').get(0).reset();
							}
							btn.button('reset');
						}else if(msg != null){
							messenger.update({
								message: msg,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
						}
				      },
				      error:function(msg){
				      	messenger.update({
							message: '修改异常，请联系管理员',
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
						btn.button('reset');
				      }
				    } );
					
				}); // 上传奇葩表单展示
	    });
		$('#logout').click(function () {
			window.location.href="logout";
		});
	});
    </script>
