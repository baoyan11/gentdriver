<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/gallery.css" />
		<link rel="stylesheet" href="css/buttons.css" />
		<link rel="stylesheet" href="css/load-more.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
		
		<script src="jquery/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="javascript/gallery-scroll.js"></script>
		<script src="javascript/unslider.js"></script>
		<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
		
		<title>文明车主</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
	<body>
	<div class="content1" id="qipaxiangce">
	  <nav class="navbar" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <a class="navbar-brand brand" href="index.html"><img src="images/logo3.png" class="logo"/>文明车主</a>
	    </div>
	   </div>
	   </nav>
	  <div class="container">
	   <h1>奇葩相册</h1>
	   <h3>“不规范停车可能会被上传哦！”</h3>
	   </div>
	</div>
	
	<div>
	<nav id="subhero-navbar" class="navbar navbar-static-top" role="navigation" style="top: 0px;">
		<div class="container">
			<div class="col-md-8">
				<ol class="breadcrumb">
	  				<li><a href="index">首页</a></li>
	                <li><a href="#qipaxiangce">回到顶部</a></li>
				</ol>
			</div>
			<div class="col-md-4">
				<div class="navbar-form">
					<div id="searchPresentations" class="form-group">
						<input type="text" class="form-control" style="width:230px;" id="search_input" placeholder="请输入车牌号" required="required">
					</div>
	                <label id="search_btn" class="btn btn-success">查询奇葩</label>
				</div>
			</div>
		</div>
	</nav>
	</div>
	
	<div class="gallery-thumbnail">
		<div id="gallery-result"></div>
		<div class="row">
			<span id="btn_loadmore" style="display:none;text-align:center;margin-bottom: 15px;">
				<button style="width: 108px;" type="button" id="load_more" class="getmore btn btn-default btn-outline btn-back-white smooth color" data-page="1" data-text="更 多" data-loading-text="加载中…" data-complete-text="没有更多了">更 多</button>
			</span>
		</div>
	</div>
	
	
	<section class="content2">
	<p>Copyright © 2014 gentdriver.com 沪ICP备14037150号</p>  
	</section>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog" style="width:940px;">
	    <div class="modal-content" id="gallery-detail">
	      
	    </div>
	  </div>
	</div>
	</body>
	<script type="text/javascript">
	$.fn.transGallery=function(gallery){
		if(gallery == undefined || gallery == null){
			return "";
		}
		var table = '';
		for(var i = 0; i < gallery.length; i ++) {
			var result = gallery[i];
			
			var detail = '';
			detail += result.id + '/' + result.cardArea + result.cardNo + '/' + result.pictures.length + '/' + result.createDate + '/' + result.description + "/";
			for(j = 0; j < result.pictures.length; j++){
				detail += result.pictures[j].thumbPathLarge;
				if(j < result.pictures.length - 1)
					detail += "-";
			}
			
			if(i%4 == 0){
				table += '<div id="gallery-row" class="row">';
			}
			table += 
				'<div class="col-xs-6 col-md-3">' + 
					'<div class="thumbnail">' + 
						//'<img data-src="holder.js/100%180" src="uploaded/' + result.pictures[0].thumbPath+ '" >' +
	                    '<img class="modalTag" index="' + i +'" data-src="holder.js/100%180" style="cursor:pointer;background-image:url(uploaded/'
	                          + result.pictures[0].thumbPath + ');background-repeat:no-repeat;background-position:center;height:200px;width:100%;" onclick=detail("' + detail + '") />' +
						'<p class="text1">' + result.cardArea + result.cardNo + '<span class="text4">(' + result.pictures.length + '张图)</span><span class="text3">' + result.createDate + '</span></p>' + 
						'<p class="text2" style="word-wrap:break-word; word-break:normal;overflow:auto;">' + cutStr(result.description,30) + '</p>' +
					'</div>' +
				'</div>';
			if(i%4 == 3 || i == gallery.length - 1){
				table += '</div>';
			}
		}
		return table;
	}
	$(document).ready(function(){
		var galleryInit = <%=request.getAttribute("gallery")%>;
		var searchInit = '${search}';
		if(galleryInit == null && searchInit == ''){
			$('#load_more').attr('data-search', '');
			$.ajax({
		      url: 'galleryAjax',
		      type: 'POST',
		      data: {search:'', pages:0, pageSize:8},
		      success:function(data){
		      	if(data.error != null) {
		      		$("#gallery-result").append('<div style="text-align:center;padding-bottom: 20px;"><img src="images/commend.gif"/></div>');
					$('#load_more').button('complete');
				} else {
					var gallery = data.results;
					var table = $(this).transGallery(gallery);
					$("#gallery-result").append(table);
					if(gallery.length < 8){
						$('#load_more').button('complete');
					}else{
						$('#load_more').attr('data-page', 1);
						$('#load_more').button('reset');
		      			$('#btn_loadmore').css("display","block");
					}
				}
		      },
		      error:function(msg){
					$('#load_more').button('complete');
		      }
			});  // 奇葩展示 
		}else if(galleryInit == null && searchInit != ''){
		    $("#gallery-result").append('<div style="text-align:center;padding-bottom: 20px;"><img src="images/commend.gif"/></div>');
			$('#load_more').button('complete');
		}else if(galleryInit != null){
			$('#load_more').attr('data-search', searchInit);
			if(galleryInit.length < 8){
				$('#load_more').button('complete');
			}else{
				$('#load_more').attr('data-page', 1);
				$('#load_more').button('reset');
				$('#btn_loadmore').css("display","block");
			}
			var init = $(this).transGallery(galleryInit);
			$("#gallery-result").append(init);
		}
				
		$('#search_btn').click(function () {
			if($('#search_input').val().length == 0){
				return false;
			}
		    $('#gallery-result').html('');
		    $('#btn_loadmore').css("display","none");
			$('#load_more').attr('data-search', $('#search_input').val());
		    $.ajax( {
		      url: 'galleryAjax',
		      type: 'POST',
		      data: {search:$('#search_input').val(), pages:0, pageSize:8},
		      success:function(data){
		      	if(data.error != null) {
		      		$("#gallery-result").append('<div style="text-align:center;padding-bottom: 20px;"><img src="images/commend.gif"/></div>');
					$('#load_more').button('complete');
				} else {
					var gallery = data.results;
					var table = $(this).transGallery(gallery);
					$("#gallery-result").append(table);
					if(gallery.length < 8){
						$('#load_more').button('complete');
					}else{
						$('#load_more').attr('data-page', 1);
						$('#load_more').button('reset');
						$('#btn_loadmore').css("display","block");
					}
				}
		      },
		      error:function(msg){
				$('#load_more').button('complete');
		      }
			}); // 奇葩展示
		}); 
		
		$('#load_more').click(function () {
			if($('#load_more').text() == $('#load_more').attr('data-complete-text')){
				return false;
			}
		
			$('#load_more').button('loading');
			var pageIndex = $('#load_more').attr('data-page');
			
		    $.ajax( {
		      url: 'galleryAjax',
		      type: 'POST',
		      data: {search:$('#load_more').attr('data-search'), pages:pageIndex, pageSize:8},
		      success:function(data){
		      	if(data.error != null) {
		      		$('#btn_loadmore').css("display","none");
					$('#load_more').button('complete');
				} else {
					var gallery = data.results;
					var table = $(this).transGallery(gallery);
					$("#gallery-result").append(table);
					$('#load_more').attr('data-page', pageIndex*1+1);
					if(gallery.length < 8){
						$('#btn_loadmore').css("display","none");
						$('#load_more').button('complete');
					}else{
						$('#load_more').button('reset');
					}
				}
		      },
		      error:function(msg){
				$('#load_more').button('complete');
		      }
			}); // 奇葩展示
		}); 
	});
				
	</script>
	<script type="text/javascript"> 
	function detail(result){
	    $("#gallery-detail").html('');
	    
	    var results = result.split("/");
	    var pics = results[5].split("-");
	    
	    var detail = 	'<div class="modal-header">' + 
		        			'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + 
		        			'<h4 class="modal-title" id="myModalLabel">' + results[1] + ' (' + results[2] + '张图)<span class="text5">' + results[3] + '</span></h4>' +
		      			'</div>' +
		      			'<div class="modal-body">' +
		      				'<div style="width: 330px; height: 400px; display: inline; float: right; border-left: 1px solid #e5e5e5; padding-left: 10px; font-size: x-small;">' +
								'<div id="comment" style="width: 330px; height: 360px; overflow: auto;">' +
								'</div>' +
								'<div class="input-group" style="padding-top: 5px;">' +
									'<input id="commentContent" type="text" class="form-control" />' +
									'<div class="input-group-btn">' +
										'<button id="sendComment" class="btn btn-primary">发送</button>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="banner has-dots" style="overflow: hidden; width: 570px;">' +
	                			'<ul style="position: relative; left: -100%; height: 400px; padding-left:0px;">';
        for(var i = 0; i < pics.length; i++){
			detail += '<li style="height:400px;background-image:url(uploaded/' + pics[i] + ');background-repeat:no-repeat;background-position:center;background-size:contain;"></li>'
        }
        detail +=			'</ul>' +
						'</div>' +
	      			'</div>' +
	      			'<div class="modal-footer">' +
	      				'<p style="text-align:left;overflow:auto;">' + results[4] + '</p>' + 
	     			'</div>';
	    $("#gallery-detail").append(detail);
	    $("#myModal").modal();
	    
	    $('#sendComment').click(function () {
	    	if($('#commentContent').val().length == 0){
	    		return false;
	    	}
	    	var messenger = Messenger({extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',theme:'ice'}).post().hide();
	    	$.ajax( {
				      url: 'comment/add',
				      type: 'POST',
				      data: {uploadID:results[0],content:$('#commentContent').val()},
				      success:function(msg){
				      	if(msg.result == null && msg.error != null){ 
				      		messenger.update({
								message: msg.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else if(msg.result == null && msg.error == null && msg != null){
				      		var dataObj=eval("("+msg+")");
				      		messenger.update({
								message: dataObj.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else{
				      		messenger.update({
								message: '评论成功',
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
							$('#commentContent').val('');
							var commentAppend = '';
							commentAppend += '<div	style="padding-bottom: 10px; margin-bottom: 12px; border-bottom: 1px solid #e5e5e5; font-family: "Microsoft YaHei", "微软雅黑", helvetica, arial, verdana, tahoma, sans-serif;">' +
												'<p style="color: #06a7e1;">' + msg.result.ip + '</p>' +
												'<div class="con">' +
													'<div style="margin-bottom: 10px; width: 300px;">' +
														'<p>' + msg.result.content + '</p>' +
													'</div>' +
													'<span style="color: #909090;">' + msg.result.createDate + '</span>' +
												'</div>' + 
											'</div>';
							if($('#comment > div:first-child').size() == 0){
								$('#comment').append(commentAppend);
							}else{
								$('#comment > div:first-child').before(commentAppend);
							}
				      	}
				      }
				     });
	    });
	     
		$('.banner').unslider({
			speed: 500,               //  The speed to animate each slide (in milliseconds)
			delay: 3000,              //  The delay between slide animations (in milliseconds)
			complete: function() {},  //  A function that gets called after every slide animation
			keys: true,               //  Enable keyboard (left, right) arrow shortcuts
			dots: true,               //  Display dot navigation
			fluid: false              //  Support responsive design. May break non-responsive designs
		});
	    
	    $.ajax( {
		      url: 'comment/get',
		      type: 'GET',
		      data: {uploadID:results[0]},
		      success:function(data){
		      	if(data.error != null) {
				} else {
					var comments = data.results;
					var comTab = '';
					for(var i = 0; i < comments.length; i ++) {
						var com = comments[i];
						comTab += '<div	style="padding-bottom: 10px; margin-bottom: 12px; border-bottom: 1px solid #e5e5e5; font-family: "Microsoft YaHei", "微软雅黑", helvetica, arial, verdana, tahoma, sans-serif;">' +
										'<p style="color: #06a7e1;">' + com.ip + '</p>' +
										'<div class="con">' +
											'<div style="margin-bottom: 10px; width: 300px;">' +
												'<p>' + com.content + '</p>' +
											'</div>' +
											'<span style="color: #909090;">' + com.createDate + '</span>' +
										'</div>' + 
									'</div>';
					}
					$('#comment').append(comTab);
				}
		      }
			});
	    
	}
	function cutStr(str, len){
	    if(str.length > len){
	          return str.substring(0,len)+"......";
	     }
	     return str;
	}
	$(window).keydown(function(event){
		    switch(event.keyCode) {
		    	case 13:
		    	if($('#search_input').is(":focus")){
					$('#search_btn').click();
				}
		    	if($('#commentContent').is(":focus")){
					$('#sendComment').click();
				}
		    }
		}); 
    </script> 
	
</html>
