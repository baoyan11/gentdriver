package cn.moonspace.flowerowner.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.moonspace.flowerowner.exception.DriverException;
import cn.moonspace.flowerowner.form.UploadForm;

@Controller
public class UploadController extends BaseController {
	
//	@RequestMapping(value = "/upload/picture", method = RequestMethod.GET)
//	public String uploadPage(ModelMap modelMap) {
//		modelMap.addAttribute("uploadForm", new UploadForm());			// 上传表单绑定
//		modelMap.addAttribute(PAGE_PATH, "upload");
//		return LAYOUT_PAGE;
//	}

	@RequestMapping(value = "/upload/picture", method = RequestMethod.POST)
	public @ResponseBody Object handleFileUpload(@Valid @ModelAttribute("uploadForm") UploadForm uploadForm, HttpServletRequest request) throws DriverException {
		String realPath = request.getSession().getServletContext().getRealPath(UPLOAD_PICTURE_PATH);
		log.info("Upload " + uploadForm.getFiles().length + " files， " + uploadForm.getCardArea() + uploadForm.getCardNo());
		return pictureService.uploadPictures(uploadForm.getFiles(), uploadForm.getCardNo(), uploadForm.getCardArea(), uploadForm.getDescription(), realPath);
	}
}
