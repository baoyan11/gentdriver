package cn.moonspace.flowerowner.controller;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.moonspace.flowerowner.exception.DriverException;


@Controller
public class GalleryController extends BaseController {
	
	/**
	 * 奇葩展示页面
	 * 
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value="/gallery", method=RequestMethod.GET) 
	public String gallery(ModelMap modelMap) throws DriverException {
		return "gallery";
	}

	/**
	 * 奇葩展示页面
	 * 
	 * @param modelMap
	 * @return
	 * @throws CloneNotSupportedException 
	 * @throws DriverException 
	 */
	@RequestMapping(value="/gallery", method=RequestMethod.POST) 
	public String galleryInit(ModelMap modelMap, @RequestParam String search,
			@RequestParam int pages, @RequestParam int pageSize) throws DriverException {
		JSONObject json = uploadService.findUploadsForFront(search, pages, pageSize, null);
		modelMap.put("gallery", json.get("results"));
		modelMap.put("search", search);
		return "gallery";
	}
	
	@RequestMapping(value="/galleryAjax", method=RequestMethod.POST) 
	public @ResponseBody Object galleryByCond(ModelMap modelMap, @RequestParam String search,
			@RequestParam int pages, @RequestParam int pageSize) throws DriverException {
		modelMap.put("search", search);
		return uploadService.findUploadsForFront(search, pages, pageSize, null);
	}
	
	@RequestMapping(value="/gallery/app", method=RequestMethod.POST) 
	public @ResponseBody Object galleryByDate(ModelMap modelMap, @RequestParam String search,
			@RequestParam int pages, @RequestParam int pageSize, @RequestParam String dateStr, @RequestParam String dateFormat) throws DriverException {
		modelMap.put("search", search);
		return uploadService.findUploadsForApp(search, pages, pageSize, dateStr, dateFormat, "yyyy-MM-dd HH:mm:ss");
	}
}
