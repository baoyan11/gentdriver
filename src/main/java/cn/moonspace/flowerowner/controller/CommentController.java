package cn.moonspace.flowerowner.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.moonspace.common.util.NetworkUtil;
import cn.moonspace.flowerowner.exception.DriverException;

@Controller
public class CommentController extends BaseController {

	@RequestMapping(value = "/comment/get", method = RequestMethod.GET)
	public @ResponseBody Object getComments(@RequestParam Long uploadID, HttpServletRequest request) throws DriverException {
		return commentService.getCommentsByUpload(uploadID);
	}
	
	@RequestMapping(value = "/comment/add", method = RequestMethod.POST)
	public @ResponseBody Object addComment(@RequestParam Long uploadID, @RequestParam String content, HttpServletRequest request) throws DriverException {
		return commentService.addComment(NetworkUtil.getIpAddr(request), content, uploadID);
	}
}
