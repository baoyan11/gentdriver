package cn.moonspace.flowerowner.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.Car;
import cn.moonspace.flowerowner.entities.Regulation;

public interface RegulationRepository extends JpaRepository<Regulation, Long> {

	@Query("from Regulation r where r.seq = :seq")
	Regulation findBySeq(@Param("seq") String seq);

	@Query("from Regulation r where r.car = :car and status != '已处理'")
	List<Regulation> findUnProcessedReglByCar(@Param("car") Car car);
	
}
