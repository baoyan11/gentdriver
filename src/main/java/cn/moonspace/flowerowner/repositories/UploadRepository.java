package cn.moonspace.flowerowner.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.Upload;

public interface UploadRepository extends JpaRepository<Upload, Long> {

	@Query("from Upload u where u.status = :status")
	Page<Upload> findUploadsByStatus(@Param("status") int status, Pageable pageable);

	@Query("from Upload u where u.cardNo like :cardNo and u.status = :status")
	Page<Upload> findUploadsByMultiConds(@Param("cardNo") String cardNo, @Param("status") int status, Pageable pageable);

	@Query("from Upload u where u.cardArea = :cardArea and u.cardNo = :cardNo and u.status = :status")
	Page<Upload> findUploadsByMultiConds(@Param("cardArea") String cardArea, @Param("cardNo") String cardNo, @Param("status") int status, Pageable pageable);

	@Query("from Upload u where u.status = :status and u.createDate < :createDate")
	Page<Upload> findUploadsByStatus(@Param("status") int status, @Param("createDate") Date createDate, Pageable pageable);

	@Query("from Upload u where u.cardNo like :cardNo and u.status = :status and u.createDate < :createDate")
	Page<Upload> findUploadsByMultiConds(@Param("cardNo") String cardNo, @Param("status") int status, @Param("createDate") Date createDate, Pageable pageable);

	@Query("from Upload u where u.cardArea = :cardArea and u.cardNo = :cardNo and u.status = :status and u.createDate < :createDate")
	Page<Upload> findUploadsByMultiConds(@Param("cardArea") String cardArea, @Param("cardNo") String cardNo, @Param("status") int status, @Param("createDate") Date createDate, Pageable pageable);
	
}

