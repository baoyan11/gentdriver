package cn.moonspace.flowerowner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query("from User r where r.username = :username")
	User findByUsername(@Param("username") String username);
	
}
