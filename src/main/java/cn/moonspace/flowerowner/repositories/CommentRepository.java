package cn.moonspace.flowerowner.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.Comment;
import cn.moonspace.flowerowner.entities.Upload;

public interface CommentRepository extends JpaRepository<Comment, Long> {

	@Query("from Comment c where c.upload = :upload")
	List<Comment> findCommentsByUpload(@Param("upload") Upload upload, Sort sort);
	
}
