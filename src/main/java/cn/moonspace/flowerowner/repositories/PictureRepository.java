package cn.moonspace.flowerowner.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cn.moonspace.flowerowner.entities.Picture;

public interface PictureRepository extends JpaRepository<Picture, Long> {

	@Query("from Picture p where p.thumbPathLarge is null")
	List<Picture> findPicturesWithoutThumbPathLarge();
	
}
