package cn.moonspace.flowerowner.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "UPLOAD")
public class Upload extends SuperEntity implements Cloneable{

	public static int STATUS_ORG = 0;
	public static int STATUS_PASS = 1;
	public static int STATUS_FAIL = 2;
	
	@Column(name = "CARD_AREA", length = 2)		// 地区号，如：沪、浙
	private String cardArea;
	@Column(name = "CARD_NO", length = 10)		// 车牌号：如：A6S359
	private String cardNo;
	@Column(name = "DESCRIPTION", length = 200)
	private String description;
	@Column(name = "CREATE_DATE")
	private Date createDate;
	@Column(name = "STATUS")					// 状态，分为：0-未审核，1-审核通过，2-审核未通过
	private int status;
	@Column(name = "CHECK_TIME")
	private Date checkTime;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private User user;
	
	@OneToMany(mappedBy = "upload")
	private List<Picture> pictures = new ArrayList<Picture>();
	
	@OneToMany(mappedBy = "upload")
	private List<Comment> comments = new ArrayList<Comment>();

	public String getCardArea() {
		return cardArea;
	}

	public void setCardArea(String cardArea) {
		this.cardArea = cardArea;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public Upload clone() throws CloneNotSupportedException {
		Upload upload = new Upload();
		upload.setCardArea(cardArea);
		upload.setCardNo(cardNo);
		upload.setCheckTime(checkTime);
		upload.setCreateDate(createDate);
		upload.setDescription(description);
		upload.setId(getId());
		upload.setStatus(status);
		upload.setUser(null);
		List<Picture> pictures = new ArrayList<Picture>();
		for(Picture picture : this.getPictures()){
			pictures.add(picture.clone());
		}
		upload.setPictures(pictures);
//		List<Comment> comments = new ArrayList<Comment>();
//		for(Comment comment : this.getComments()){
//			comments.add(comment.clone());
//		}
//		upload.setComments(comments);
		return upload;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
