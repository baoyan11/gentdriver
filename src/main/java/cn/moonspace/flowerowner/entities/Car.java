package cn.moonspace.flowerowner.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CAR")
public class Car extends SuperEntity {
	
	public static String[] AREA_SET
		= new String[]{"京","沪","港","吉","鲁","冀","湘","青","苏","浙",
						"粤","台","甘","川","黑","蒙","新","津","渝","澳",
						"辽","豫","鄂","晋","皖","赣","闽","琼","陕","云",
						"贵","藏","宁","桂"};
	
	@Column(name = "CARD_AREA", length = 2)		// 地区号，如：沪、浙
	private String cardArea;
	@Column(name = "CARD_NO", length = 10)		// 车牌号：如：A6S359
	private String cardNo;
	@Column(name = "MACHINE_NO", length = 20)	// 发送机号
	private String machineNo;
	@Column(name = "CARD_TYPE", length = 20)	// 车辆类型
	private String cardType;
	@Column(name = "IS_REAL")	// 车辆类型
	private Boolean isReal;
	@Column(name = "DESCRIPTION", length = 50)	// 备注
	private String description;
	
	@ManyToMany
	private List<User> users = new ArrayList<User>();
	@OneToMany(mappedBy = "car")
	private List<Regulation> regulations = new ArrayList<Regulation>();

	public String getCardArea() {
		return cardArea;
	}

	public void setCardArea(String cardArea) {
		this.cardArea = cardArea;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getMachineNo() {
		return machineNo;
	}

	public void setMachineNo(String machineNo) {
		this.machineNo = machineNo;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Regulation> getRegulations() {
		return regulations;
	}
	
	public void setRegulations(List<Regulation> regulations) {
		this.regulations = regulations;
	}

	public Boolean getIsReal() {
		return isReal;
	}

	public void setIsReal(Boolean isReal) {
		this.isReal = isReal;
	}

	@Override
	public Car clone() throws CloneNotSupportedException {
		Car car = new Car();
		car.setId(getId());
		car.setCardArea(getCardArea());
		car.setCardNo(getCardNo());
		car.setCardType(getCardType());
		car.setDescription(getDescription());
		car.setMachineNo(getMachineNo());
		car.setIsReal(getIsReal());
		return car;
	}
}
