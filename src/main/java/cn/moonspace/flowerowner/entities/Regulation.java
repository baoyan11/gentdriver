package cn.moonspace.flowerowner.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="REGULATION")
public class Regulation extends SuperEntity {
	
	@Column(name="SEQ", length = 100)	
	private String seq;
	@Column(name="TIME")
	private String time;
	@Column(name="ADDRESS")
	private String address;
	@Column(name="DEPT")
	private String dept;
	@Column(name="CONTENT")
	private String content;
	@Column(name="RULE")
	private String rule;
	@Column(name="STATUS")
	private String status;
	
	@Column(name="CARD_AREA")
	private String cardArea;
	@Column(name="CARD_NUMBER")
	private String cardNumber;
	@Column(name="CARD_TYPE")
	private String cardType;
	
	@ManyToOne
	@JoinColumn(name="CAR_ID")
	private Car car;
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCardArea() {
		return cardArea;
	}
	public void setCardArea(String cardArea) {
		this.cardArea = cardArea;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public Car getCar() {
		return car;
	}

	@Override
	public Regulation clone() throws CloneNotSupportedException {
		Regulation regulation = new Regulation();
		regulation.setId(getId());
		regulation.setAddress(getAddress());
		regulation.setCardArea(getCardArea());
		regulation.setCardNumber(getCardNumber());
		regulation.setCardType(getCardType());
		regulation.setContent(getContent());
		regulation.setDept(getDept());
		regulation.setRule(getRule());
		regulation.setSeq(getSeq());
		regulation.setStatus(getStatus());
		regulation.setTime(getTime());
		return regulation;
	}
}
