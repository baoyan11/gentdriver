package cn.moonspace.flowerowner.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PICTURE")
public class Picture extends SuperEntity implements Cloneable{

	@Column(name = "IMAGE_PATH")
	private String imagePath;
	@Column(name = "THUMB_PATH")
	private String thumbPath;
	@Column(name = "THUMB_PATH_LARGE")
	private String thumbPathLarge;
	@Column(name = "CREATE_DATE")
	private Date createDate;
	
	@ManyToOne
	@JoinColumn(name="UPLOAD_ID")
	private Upload upload;
	
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Upload getUpload() {
		return upload;
	}
	
	public void setUpload(Upload upload) {
		this.upload = upload;
	}

	public String getThumbPath() {
		return thumbPath;
	}

	public void setThumbPath(String thumbPath) {
		this.thumbPath = thumbPath;
	}

	public String getThumbPathLarge() {
		return thumbPathLarge;
	}

	public void setThumbPathLarge(String thumbPathLarge) {
		this.thumbPathLarge = thumbPathLarge;
	}
	
	@Override
	public Picture clone() throws CloneNotSupportedException {
		Picture picture = new Picture();
		picture.setCreateDate(createDate);
		picture.setId(getId());
		picture.setImagePath(imagePath);
		picture.setThumbPath(thumbPath);
		picture.setThumbPathLarge(thumbPathLarge);
		return picture;
	}
}
