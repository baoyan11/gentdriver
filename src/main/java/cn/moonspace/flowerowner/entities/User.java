package cn.moonspace.flowerowner.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User extends SuperEntity {

	@Column(name = "USERNAME", nullable = false, unique = true, length = 50)
	private String username;
	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "EMAIL")
	private String email;
	@Column(name = "MOBILE")
	private String mobile;
	@Column(name = "QQ")
	private String qq;
	@Column(name = "WEIXIN")
	private String weixin;
	@Column(name = "WEIBO")
	private String weibo;

	/**驾驶员档案信息**/
	@Column(name= "DRIVER_AREA")
	private String driverArea;	// 驾驶员档案地区
	@Column(name= "DRIVER_NO")
	private String driverNo;	// 驾驶员档案编号
	
	@Column(name = "CREATE_DATE")
	private Date createDate;	// 注册时间
	@Column(name = "LAST_LOGIN_DATE")
	private Date lastLoginDate;	// 最后登录时间
	
	@OneToMany(mappedBy = "user")
	private List<Upload> uploads = new ArrayList<Upload>();
	@ManyToMany(mappedBy = "users")
	private List<Car> cars = new ArrayList<Car>();

	public String getDriverArea() {
		return driverArea;
	}

	public void setDriverArea(String driverArea) {
		this.driverArea = driverArea;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

	public List<Upload> getUploads() {
		return uploads;
	}
	
	public void setUploads(List<Upload> uploads) {
		this.uploads = uploads;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getWeibo() {
		return weibo;
	}

	public void setWeibo(String weibo) {
		this.weibo = weibo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

}
