package cn.moonspace.flowerowner.form;

import org.hibernate.validator.constraints.NotBlank;

import cn.moonspace.common.util.BaseBean;

@SuppressWarnings("serial")
public class DriverForm extends BaseBean {
	
	@NotBlank(message = "驾驶员档案编号不能为空！")
	private String driverNo;

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

}
