package cn.moonspace.flowerowner.form;

import org.springframework.web.multipart.MultipartFile;

import cn.moonspace.common.util.BaseBean;

@SuppressWarnings("serial")
public class UploadForm extends BaseBean{
	
	private String cardNo;
	private String cardArea;
	private String description;
	private MultipartFile[] files;
	
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardArea() {
		return cardArea;
	}
	public void setCardArea(String cardArea) {
		this.cardArea = cardArea;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public MultipartFile[] getFiles() {
		return files;
	}
	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}
}
