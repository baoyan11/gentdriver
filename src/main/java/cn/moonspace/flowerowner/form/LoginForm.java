package cn.moonspace.flowerowner.form;

import org.hibernate.validator.constraints.Length;

import cn.moonspace.common.util.BaseBean;

/**
 * 登录表单
 *  
 * @author didi
 *
 * 创建时间：2013-1-8
 *
 */
@SuppressWarnings("serial")
public class LoginForm extends BaseBean {

	@Length(max = 20, message = "用户名长度最大20位")
	private String username;
	@Length(max = 20, message = "密码长度最大20位")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}