package cn.moonspace.flowerowner.form;

import org.hibernate.validator.constraints.NotEmpty;

import cn.moonspace.common.util.BaseBean;

/**
 * 查违章表单
 * 
 * @author diyongchao
 *
 */
@SuppressWarnings("serial")
public class RegulationForm extends BaseBean {

	private String area;
	@NotEmpty(message="车牌号不能为空！")	
	private String cardNo;
	private String cardType;
	@NotEmpty(message="发动机号不能为空！")	
	private String machineNo;

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMachineNo() {
		return machineNo;
	}

	public void setMachineNo(String machineNo) {
		this.machineNo = machineNo;
	}

}
