package cn.moonspace.flowerowner.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import cn.moonspace.common.util.BaseBean;

/**
 * 注册表单
 * 
 * @author didi
 *
 * 创建时间：2013-1-8
 *
 */
@SuppressWarnings("serial")
public class RegisterForm extends BaseBean {

	@Length(min = 6, max = 20, message = "用户名长度最小6位，最大12位")
	private String username;
	@Length(min = 6, max = 20, message = "密码长度最小6位，最大12位")
	private String password;
	@Length(min = 6, max = 20, message = "确认密码长度最小6位，最大12位")
	private String repassword;
	
	@Email(message = "邮箱格式不正确")
	@Length(max = 200, message = "邮箱长度不可超过200位")
	private String email;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}