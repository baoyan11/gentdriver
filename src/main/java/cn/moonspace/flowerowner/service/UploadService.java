package cn.moonspace.flowerowner.service;

import net.sf.json.JSONObject;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.exception.DriverException;

public interface UploadService {

	@Transactional(readOnly=true)
	JSONObject findUploadsForFront(int pages, int pageSize, String dstDateFormat) throws DriverException;

	@Transactional(readOnly=true)
	JSONObject findUploadsForFront(String search, int pages, int pageSize, String dstDateFormat) throws DriverException;

	@Transactional(readOnly=true)
	JSONObject findUploadsForApp(String search, int pages, int pageSize, String dateStr, String orgDateFormat, String dstDateFormat) throws DriverException;
	
	@Transactional
	JSONObject findUploadsForBack(Integer status, int pages, int pageSize, String dstDateFormat) throws DriverException;

	@Transactional
	JSONObject updateUploadStatus(Long id, int status);
	
	@Transactional
	JSONObject delUpload(Long id, String realPath);
}
