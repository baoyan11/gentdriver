package cn.moonspace.flowerowner.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cn.moonspace.common.util.ImageUtil;
import cn.moonspace.flowerowner.entities.Picture;
import cn.moonspace.flowerowner.entities.Upload;
import cn.moonspace.flowerowner.exception.DriverException;
import cn.moonspace.flowerowner.service.PictureService;

import javax.imageio.ImageIO;

@Service("pictureService")
public class PictureServiceImpl extends BaseService implements PictureService { 

	@Override
	public JSONObject uploadPictures(MultipartFile[] files, String cardNo,
			String cardArea, String description, String realPath) throws DriverException {
		JSONObject result = new JSONObject();
		
		Upload upload = new Upload();
		upload.setCardArea(cardArea);
		upload.setCardNo(cardNo);
		upload.setCreateDate(new Date());
		upload.setStatus(Upload.STATUS_ORG);
		upload.setDescription(description);
		
		for (MultipartFile file : files) {
			String basePath = file.getName() + "_" + new Date().getTime();

			// 保存原图，加水印
			String imagePath = basePath + ImageUtil.SUFFIX_JPG;
			File imageFile = new File(realPath, imagePath);
            File waterFile = new File(
                    imageFile.getParentFile().getParent()
                    + "/images/watermark.png");
            try {
				Thumbnails.of(file.getInputStream()).scale(1.0).toFile(imageFile);
			} catch (IOException e) {
				DriverException de = new DriverException(DriverException.ThumbnailException, "Thumbnail Error: " + e.getMessage(), this.getClass().getName());
				de.setStackTrace(e.getStackTrace());
				throw de;
			}
            //FileUtils.copyInputStreamToFile(file.getInputStream(), imageFile);

            // 保存缩略图
			String thumbPath = basePath + ImageUtil.SUFFIX_JPG_THUMB;
			String thumbPathLarge = basePath + ImageUtil.SUFFIX_JPG_THUMB_LARGE;
            try {
				Thumbnails.of(imageFile).size(242, 200).outputQuality(0.9f)
				        .toFile(realPath + "/" + thumbPath);
				Thumbnails.of(imageFile).size(570, 400).outputQuality(0.9f)
		        		.watermark(Positions.BOTTOM_RIGHT, ImageIO.read(waterFile),0.5f)
		        		.toFile(realPath + "/" + thumbPathLarge);
			} catch (IOException e) {
				DriverException de = new DriverException(DriverException.ThumbnailException, "Thumbnail Error: " + e.getMessage(), this.getClass().getName());
				de.setStackTrace(e.getStackTrace());
				throw de;
			}
            //ImageUtil.makeSmallImage(imageFile, realPath, thumbPath, 242, 200);
            
			Picture picture = new Picture();
			picture.setImagePath(imagePath);
			picture.setThumbPath(thumbPath);
			picture.setThumbPathLarge(thumbPathLarge);
			picture.setCreateDate(new Date());
			picture.setUpload(upload);
			pictureRepository.save(picture);
		}
		upload = uploadRepository.save(upload);
		result.put("message", "成功上传" + files.length + "张奇葩照");
		return result;
	}
	
	
}
