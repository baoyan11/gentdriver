package cn.moonspace.flowerowner.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import cn.moonspace.common.util.Md5Util;
import cn.moonspace.flowerowner.entities.User;
import cn.moonspace.flowerowner.form.LoginForm;
import cn.moonspace.flowerowner.service.UserService;

@Service("userService")
public class UserServiceImpl extends BaseService implements UserService {
	
	
	@Override
	public User save(User user) {
		log.debug("Save user : " + user);
		user.setPassword(Md5Util.MD5(user.getPassword()));
		user.setCreateDate(new Date());
		log.debug("MD5 password : " + user.getPassword());
		return userRepository.save(user);
	}

	@Override
	public int login(LoginForm loginForm) {
		User u = userRepository.findByUsername(loginForm.getUsername());
		if(null == u) {
			return USERNAME_NOT_FOUND;
		} else if(u.getPassword().equals(Md5Util.MD5(loginForm.getPassword()))){
			u.setLastLoginDate(new Date());
			userRepository.save(u);
			return LOGIN_SUCCESS;
		} else {
			return PASSWORD_ERROR;
		}
	}

}
