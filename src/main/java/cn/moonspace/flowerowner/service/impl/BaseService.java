package cn.moonspace.flowerowner.service.impl;

import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import cn.moonspace.flowerowner.repositories.CarRepository;
import cn.moonspace.flowerowner.repositories.CommentRepository;
import cn.moonspace.flowerowner.repositories.ManagerRepository;
import cn.moonspace.flowerowner.repositories.PictureRepository;
import cn.moonspace.flowerowner.repositories.RegulationRepository;
import cn.moonspace.flowerowner.repositories.UploadRepository;
import cn.moonspace.flowerowner.repositories.UserRepository;

public class BaseService {
	
	protected Log log = LogFactory.getLog(getClass());

	protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	protected UserRepository userRepository;
	@Autowired
	protected CarRepository carRepository;
	@Autowired
	protected PictureRepository pictureRepository;
	@Autowired
	protected RegulationRepository regulationRepository;
	@Autowired
	protected UploadRepository uploadRepository;
	@Autowired
	protected ManagerRepository managerRepository;
	@Autowired
	protected CommentRepository commentRepository;
	
}
