package cn.moonspace.flowerowner.service;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.entities.Manager;
import cn.moonspace.flowerowner.form.LoginForm;

public interface ManagerService {

	static final int LOGIN_SUCCESS = 1;
	static final int PASSWORD_ERROR = -1;
	static final int USERNAME_NOT_FOUND = -2;

	static final int CHANGE_SUCCESS = 1;
	static final int CHANGE_ERROR = -1;

	@Transactional
	int login(LoginForm loginForm);
	
	@Transactional
	int changePass(Manager manager, String oldPass, String newPass);

}
