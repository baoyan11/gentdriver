package cn.moonspace.flowerowner.service;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.entities.User;
import cn.moonspace.flowerowner.form.LoginForm;

public interface UserService {

	static final int LOGIN_SUCCESS = 1;
	static final int PASSWORD_ERROR = -1;
	static final int USERNAME_NOT_FOUND = -2;

	@Transactional
	User save(User user);

	@Transactional
	int login(LoginForm loginForm);

}
