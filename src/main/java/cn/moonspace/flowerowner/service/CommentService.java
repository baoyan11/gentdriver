package cn.moonspace.flowerowner.service;

import net.sf.json.JSONObject;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.exception.DriverException;

public interface CommentService {

	@Transactional
	JSONObject addComment(String ip, String content, Long uploadID) throws DriverException;
	
	@Transactional
	JSONObject getCommentsByUpload(Long uploadID) throws DriverException;

}
