package cn.moonspace.flowerowner.exception;

public class DriverException extends Exception {

	public DriverException() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 9060298170261976027L;

	// MSG_CODE description...
	public final static String Success = "0";

	public final static String SearchRegulationException = "1001";	
	public final static String SearchDriverException = "1002";	

	public final static String UploadCloneException = "2001";		
	public final static String ThumbnailException = "2002";	
	public final static String CommentCloneException = "2003";
	public final static String CarCloneException = "2004";
	public final static String RegulationCloneException = "2005";
	
	public final static String DateParseException = "3001";
	
	//异常代码
	private String code;
	
	//异常信息
	private String description;
	
	//异常发生类
	private String className;
	
	public DriverException(String code, String description, String className) {
		super(description);
		setCode(code);
		setClassName(className);
		setDescription(description);
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}	
}
