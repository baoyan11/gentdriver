package cn.moonspace.common.util;

import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

public class MyJsonUtil {
	public static JsonConfig getConfig(){
		JsonConfig config = new JsonConfig();
		config.registerJsonValueProcessor(Date.class , new MyJsonValueProcessor());  
        config.setJsonPropertyFilter(new PropertyFilter(){
            @Override
            public boolean apply(Object source, String name, Object value) {
                return value == null;
            }
        });
        return config;
	}

	public static JsonConfig getConfig(String dateFormat){
		if(dateFormat == null || dateFormat.length() <= 0){
			return getConfig(); 
		}
		JsonConfig config = new JsonConfig();
		config.registerJsonValueProcessor(Date.class , new MyJsonValueProcessor(dateFormat));  
        config.setJsonPropertyFilter(new PropertyFilter(){
            @Override
            public boolean apply(Object source, String name, Object value) {
                return value == null;
            }
        });
        return config;
	}
}
